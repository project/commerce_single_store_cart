<?php

namespace Drupal\commerce_single_store_cart\Resolver;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_store\Resolver\StoreResolverInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns the product's default store, when a product is present in the URL.
 *
 * Ensures that the current store is always correct when viewing or editing the
 * product.
 */
class CookieStoreResolver implements StoreResolverInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new CookieStoreResolver object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(RequestStack $request_stack, EntityTypeManagerInterface $entity_type_manager) {
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve() {
    $request = $this->requestStack->getCurrentRequest();
    $current_store = $request->cookies->get('current_store');
    $store = NULL;
    if ($current_store) {
      $storage = $this->entityTypeManager->getStorage('commerce_store');
      $store = $storage->load($current_store);
    }
    return $store;
  }

}
