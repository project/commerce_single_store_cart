<?php

namespace Drupal\commerce_single_store_cart\Resolver;

use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_store\Resolver\StoreResolverInterface;

/**
 * Returns the current cart store, if known.
 */
class CurrentCartStoreResolver implements StoreResolverInterface {

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * Constructs a new CurrentCartStoreResolver object.
   *
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   */
  public function __construct(CartProviderInterface $cart_provider) {
    $this->cartProvider = $cart_provider;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve() {
    $carts = $this->cartProvider->getCarts();
    $store = NULL;
    if (count($carts) === 1) {
      $cart = reset($carts);
      $store = $cart->getStore();
    }
    return $store;
  }

}
