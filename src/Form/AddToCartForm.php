<?php

namespace Drupal\commerce_single_store_cart\Form;

use Drupal\commerce_cart\Form\AddToCartForm as OriginalAddToCartForm;
use Drupal\commerce_cart\Form\AddToCartFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides the order item add to cart form.
 */
class AddToCartForm extends OriginalAddToCartForm implements AddToCartFormInterface {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $this->entity;
    /** @var \Drupal\commerce\PurchasableEntityInterface $purchased_entity */
    $purchased_entity = $order_item->getPurchasedEntity();
    $store = $this->selectStore($purchased_entity);

    // Check that we have only one valid cart.
    $error = FALSE;
    $carts = $this->cartProvider->getCarts();
    if (count($carts) > 1) {
      $this->messenger()->addError($this->t('You cannot buy from two different stores at the same time. Why do you have two carts?'));
      $error = TRUE;
    }
    elseif (count($carts) === 1) {
      $cart = reset($carts);
      if ($cart->getStore() !== $store && $cart->hasItems()) {
        $error = TRUE;
        $store_id = $cart->getStoreId();
        $url = Url::fromRoute(
          'commerce_single_store_cart.empty_cart_form',
          ['commerce_store' => $store_id],
          ['query' => [
            'destination' => $this->getRedirectDestination()->get(),
            'token' => \Drupal::csrfToken()->get("cart/empty/$store_id")
          ]]
        )->toString(TRUE);
        $url = $url->getGeneratedUrl();
        $this->messenger()->addWarning(
          $this->t('You cannot buy from two different stores at the same time. <a href=":emptyCart">Empty the current cart?</a>',
            [':emptyCart' => $url]));
      }
      elseif ($cart->getStore() !== $store && !$cart->hasItems()) {
        // Is empty, we can delete it and continue.
        $cart->delete();
      }
    }
    if (!$error) {
      return parent::submitForm($form, $form_state);
    }
    else {
      return NULL;
    }
  }

}
